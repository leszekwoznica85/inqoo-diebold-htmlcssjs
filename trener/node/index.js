// node -v
const http = require('http')
const fs = require('fs')

// import http from 'http';

console.log('hello node');

const server = http.createServer((req, res) => {
  console.log(req.headers)

  var data = ''

  req.on('data', chunk => data += chunk)
  req.on('end', () => {
    console.log(data);
    fs.writeFileSync('./output.txt', data)
  })

  res.write('<h1>Hello Server</h1>')
  res.end()
})

server.listen(8080, () => {
  console.log('ready')
})
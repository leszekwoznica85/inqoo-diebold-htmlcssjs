```js
function makePerson(name){
    var person = {}
    person.name = name 
    person.sayHello = function(){ return `Hi, I am ${person.name}` }
    return person;
}

alice = makePerson('Alice')
alice.sayHello()
"Hi, I am Alice"
alice 
{name: "Alice", sayHello: ƒ}

``` 

```js
Fn.apply( willBeThis, [arg1, arg2])
Fn.call( willBeThis, arg1, arg2)

fnWithThis = Fn.bind( willBeThis )
fnWithThis(arg1, arg2)

Fn = () => this /// fn = fn.bind(this)

```

```js
function makePerson(name){
//     var person = {}
    this.name = name 
    this.sayHello = function(){ return `I am ${this.name}`; }
    return this;
}

alice = makePerson.call({x:1},'Alice')
bob = makePerson.call({x:2},'Bob')
{x: 2, name: "Bob", sayHello: ƒ}
alice 
{x: 1, name: "Alice", sayHello: ƒ}
```

## New

```js
function Person(name){
//     var person = {}
    this.name = name 
    this.sayHello = function(){ return `I am ${this.name}`; }
//     return this;
}

// alice =  Person.call({},'Alice')
alice = new Person('Alice')
bob = new Person('Bob')
// Person {name: "Bob", sayHello: ƒ}

alice.sayHello()
"I am Alice"
bob.sayHello()
"I am Bob"
```


```js
function Person(name){
    this.name = name 
    this.sayHello = sayHello
}
function sayHello(){ return `I am ${this.name}`; }

// alice =  Person.call({},'Alice')
alice = new Person('Alice')
bob = new Person('Bob')

alice.sayHello === bob.sayHello
// true

```


```js
function Person(name){
    this.name = name 
}
Person.prototype.sayHello = function(){ return `I am ${this.name}`; }

alice.sayHello === bob.sayHello
true
alice 
// Person {name: "Alice"}
//   name: "Alice"
//   [[Prototype]]: Object
//     sayHello: ƒ ()
//     constructor: ƒ Person(name)
//       [[Prototype]]: Object
//       ...

alice.sayHello()
"I am Alice"
Object.getPrototypeOf(alice) === Object.getPrototypeOf(bob)
true

```

```
bob.sayHello = function(){return 'dont talk to me !'}
ƒ (){return 'dont talk to me !'}
alice.sayHello()
"I am Alice"
bob.sayHello()
"dont talk to me !"
bob 
```


```js
function Person(name){
    this.name = name 
}
Person.prototype.sayHello = function(){ return `I am ${this.name}`; }

alice = new Person('Alice')
bob = new Person('Bob')

function Employee(name, salary){
    Person.apply(this, arguments)
    this.salary = salary
}
Employee.prototype = Object.create(Person.prototype)
Employee.prototype.work = function(){ 
    return `I want ${this.salary}` 
} 
Employee.prototype.sayHello = function(){ 
    return Person.prototype.sayHello.apply(this,arguments) + ' and I work' 
} 

tom = new Employee('Tom',12000)
tom.work()
"I want 12000"
tom.sayHello()
"I am Tom"
tom instanceof Employee
true
tom instanceof Person 
true
tom instanceof Object 
true
```


```js

class Person{
    constructor(name){
        this.name = name
    }
    sayHello(){
        return `I am ${this.name}`
    }
}

class Employee extends Person{

    constructor(name,salary){
        super(...arguments)
        this.salary = salary
    }

    work(){
        return `I want ${this.salary}`
    }

    sayHello(){
        return super.sayHello() + ' and i work';
    }
    
}

tom = new Employee('Tom',12300)

// Employee {name: "Tom", salary: 12300}
//   name: "Tom"
//   salary: 12300
//   [[Prototype]]: Person
//     constructor: class Employee
//     sayHello: ƒ sayHello()
//     work: ƒ work()
//     [[Prototype]]: Object
//       constructor: class Person
//       sayHello: ƒ sayHello()
//       [[Prototype]]: Object

tom.sayHello()
"I am Tom and i work"
tom.work()
"I want 12300"
```

```js
div = document.createElement('div')
// <div>​</div>​
div.__proto__
// HTMLDivElement {Symbol(Symbol.toStringTag): "HTMLDivElement", constructor: ƒ}
div.__proto__.__proto__
// HTMLElement {…}
div.__proto__.__proto__.__proto__
// Element {…}
div.__proto__.__proto__.__proto__.__proto__
// Node {…}
div.__proto__.__proto__.__proto__.__proto__.__proto__
// EventTarget {Symbol(Symbol.toStringTag): "EventTarget", addEventListener: ƒ, dispatchEvent: ƒ, removeEventListener: ƒ, constructor: ƒ}
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__
// {constructor: ƒ, __defineGetter__: ƒ, __defineSetter__: ƒ, hasOwnProperty: ƒ, __lookupGetter__: ƒ, …}
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__
// null
```
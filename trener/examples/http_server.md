# Instalacja
npm i -g http-server

# Uruchomienie
cd twojkatalog/backoffice
http-server

<!-- http-server.cmd  <<-- PowerShell -->

-> http://localhost:8080/

## pobieramy plik products.json
```js
xhr = new XMLHttpRequest()
xhr.open('GET','products.json')
xhr.addEventListener('loadend', console.log )
xhr.send()
```

npm i -g json-server
json-server ./products.json


```js

function loadJSON(url, callback){
    const xhr = new XMLHttpRequest()
    xhr.open('GET',url)
    xhr.addEventListener('loadend', event => {
        const data = JSON.parse(event.target.responseText)
       //  console.log(3)        
        callback(data)
    })
    xhr.send()
   
    //  console.log(1)
    //  return data; // data does not exist yet
}
// data = loadJSON('products.json') // data does not exist yet
loadJSON('products.json', resp => console.log('yey',resp))
// console.log(2)

```
var testCart = new Cart();
var products = [
  { id: 123, price: 1000, promotion: true, discount: 0.1 },
  { id: 223, price: 2000, promotion: false, },
];

(function emptyCart() {
  console.log("Create cart");
  console.assert(
    testCart.cartItem.length === 0,
    "Object test cart was not creat"
  );
})();
(function addFirstProduct() {
  console.log("Add rirst product to cart");
  testCart.addProductToCart(123);
  console.assert(testCart.cartItem[0].product.id === 123, "Bad product id");
  console.assert(testCart.cartItem[0].amount === 1, "Bad amount");
  console.assert(testCart.cartItem[0].subTotal === 1107, "Bad subtotal");
  //console.log(testCart.getTotalPrice())
  console.assert(testCart.getTotalPrice() === 1107, "Bad total price");
})();
(function addTheSameProduct() {
  console.log("Add the same product to cart");
  testCart.addProductToCart(123);
  console.assert(testCart.cartItem.length === 1, "Add new product");
  console.assert(testCart.cartItem[0].amount === 2, "Bad amount");
  console.assert(testCart.cartItem[0].subTotal === 2214, "Bad subtotal");
  console.assert(testCart.getTotalPrice() === 2214, "Bad total price");
})();
(function removeAmountOfProductFromCart() {
  console.log("Remove amount of product from cart");
  testCart.removeProductFromCart(123);
  console.assert(testCart.cartItem.length === 1, "remove product");
  console.assert(testCart.cartItem[0].amount === 1, "Bad amount");
  console.assert(testCart.cartItem[0].subTotal === 1107, "Bad subtotal");
  console.assert(testCart.getTotalPrice() === 1107, "Bad total price");
})();
(function removeProductFromCart() {
  console.log("Remove product from cart");
  testCart.removeProductFromCart(123);
  console.assert(testCart.cartItem.length === 0, "remove product");
})();
(function addTwoProducts() {
  console.log("Add two products to cart");
  testCart.addProductToCart(123);
  console.assert(testCart.getTotalPrice() === 1107, "Bad total price: "+ testCart.getTotalPrice());
  //console.log(testCart.cartItem[0])
  testCart.addProductToCart(223);  
  console.assert(testCart.cartItem[0].product.id === 123, "Bad product id");
  console.assert(testCart.cartItem[0].amount === 1, "Bad amount");
  console.assert(testCart.cartItem[0].subTotal === 1107, "Bad subtotal");

 
  console.assert(testCart.cartItem[1].product.id === 223, "Bad product id");
  console.assert(testCart.cartItem[1].amount === 1, "Bad amount");
  console.assert(testCart.cartItem[1].subTotal === 2460, "Bad subtotal: " +testCart.cartItem[1].subTotal );
  console.assert(testCart.getTotalPrice() === 3567, "Bad total price");
})();
(function addTwoTheSameProducts() {
  console.log("Add two the same products to cart");
  testCart.addProductToCart(123);
  testCart.addProductToCart(223);
  console.assert(testCart.cartItem[0].product.id === 123, "Bad product id");
  console.assert(testCart.cartItem[0].amount === 2, "Bad amount");
  console.assert(testCart.cartItem[0].subTotal === 2214, "Bad subtotal");
  console.assert(testCart.cartItem[1].product.id === 223, "Bad product id");
  console.assert(testCart.cartItem[1].amount === 2, "Bad amount");
  console.assert(testCart.cartItem[1].subTotal !== 0, "Bad subtotal");
  console.assert(testCart.getTotalPrice() === 7134, "Bad total price");
})();
(function removeAmountOfProductFromCart() {
  console.log("Remove amount of product from cart");
  testCart.removeProductFromCart(123);
  //console.log(testCart.cartItem[0])
  console.assert(testCart.cartItem.length === 2, "remove product");
  console.assert(testCart.cartItem[0].amount === 1, "Bad amount");
  console.assert(testCart.cartItem[0].subTotal === 1107, "Bad subtotal");
  console.assert(testCart.getTotalPrice() === 6027, "Bad total price");
  testCart.promoValue = 500;
  console.assert(testCart.getTotalPrice() === 5527, "Bad total price:" + testCart.getTotalPrice());
})();
(function removeProductFromCart() {
  console.log("Remove product from cart");
  testCart.removeProductFromCart(123);
  console.assert(testCart.cartItem.length === 1, "remove product");
  console.assert(testCart.getTotalPrice() === 4420, "Bad total price");
})();
console.log('---------------End of the test --------------------')

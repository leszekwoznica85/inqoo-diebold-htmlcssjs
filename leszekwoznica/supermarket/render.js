function renderToDocument(product) {
    var description = getCategoryReturnDescription(
      product.description,
      product.category[0]
    );
    let discountGrossPrice = roundPrice(
      getGrossPrice(getDiscountedNettPrice(product))
  
    );
    var grossPrice = roundPrice(getGrossPrice(product.price));
    var item = `
    <div class="row js-product"> 
      <div class="col-sm">
           <img src="${product.image}" alt="">
      </div>
      <div class="col-sm">
        <h3>${product.name}</h3>
        <p>${description}</p>
      </div>
    <div class="col-sm text-right">
    </div>
    <div class="col-sm">
      <p><del>${grossPrice}</del> - ${discountGrossPrice}</p>
      <buttom class="btn btn-info js-add-product" vaulue="${product.id}">Add to cart</buttom>
    </div>
  <div>`;
    productList.innerHTML += item;
  }
  function renderCartToDocument(product) {
    
    //console.log(product);
    item = `                        
      <li class="list-group-item d-flex justify-content-between align-items-center js-product">
      <h5>${product.product.name}</h5>
      <div>${product.amount} * ${ roundPrice(getGrossPrice(getDiscountedNettPrice(product.product)))} = ${roundPrice(product.subTotal)}</div>
      <button class="btn btn-info close" vaulue="${product.product.id}">X</button>
    </li>`;
    productBasket.innerHTML += item;
  }
  function createHTML() {
    index = 0;
    //sortProduct ? console.log(sortProduct) : console.log(sortProduct)
    productsList = sortProduct === true ? products.reverse() : products;
    for (let product of productsList) {
      if (promotion_check_box === true && product.promotion === false) continue;
      if (index < productLimit) renderToDocument(product, "productsList");
  
      index++;
    }
  }
  function createCartHTML() {
    numberOfProductsInBasket.innerHTML = mainCart.cartItem.length
    totalPrice.innerHTML = roundPrice(mainCart.getTotalPrice());
    productBasket.innerHTML ="";
    for (let item of mainCart.cartItem) {
      renderCartToDocument(item);
    }
  }
var tax = 23;
var shop_discount = 0.5;

function getGrossPrice(nett_price) {
    return nett_price * (1 + tax / 100);
  }
  function roundPrice(price){

    return (price/100).toFixed(2) + '$'
  }
  function getDiscountedNettPrice(product) {
    var discount = 'number' === typeof product.discount ? product.discount : shop_discount;
    var nett_price = (product.price - (product.price * (product.promotion ? discount : 0))) ;
    return nett_price;
  }
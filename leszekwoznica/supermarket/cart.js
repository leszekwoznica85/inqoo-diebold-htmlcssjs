class Cart {
  constructor() {
    this.cartItem = [];
    this.item;
    this.totalPrice = 0;
    this.promoValue = 0;
  }
  addProductToCart(product_id) {
    this.item = undefined;
    let product = this.getProdcutFromId(parseInt(product_id, 10));
    console.assert(
      product !== undefined,
      "Wrong product id " + parseInt(product_id, 10)
    );
    this.cartItem.forEach((item) => {
      if (item.product.id === parseInt(product_id, 10)) {
        this.item = item;
      }
    });
    if (this.item === undefined) {
      this.cartItem.push({ product, amount: 1 });
      this.getSubTotalPrice(this.cartItem[this.cartItem.length - 1]);
    } else {
      this.item.amount++;
      this.getSubTotalPrice(this.item);
    }
  }
  removeProductFromCart(product_id) {
    let product = this.getProdcutFromId(parseInt(product_id, 10));
    this.cartItem.forEach((item) => {
      if (item.product.id === product.id) {
        if (item.amount > 1) {
          item.amount--;
          this.getSubTotalPrice(item);
        } else this.cartItem.splice(this.cartItem.indexOf(item), 1);
      }
    })
    /*
    for (let item of this.cartItem) {
      if (item.product.id === product.id) {
        if (item.amount > 1) {
          item.amount--;
          this.getSubTotalPrice(item);
        } else this.cartItem.splice(this.cartItem.indexOf(item), 1);
      }
    }
    */
  }
  getProdcutFromId(product_id) {

 //   products.forEach( (product) => {if(product.id === product_id) { return product} }); - nie działa
    for (let product of products) {
      if (product.id === product_id) {
        return product;
      }
    }
  }
  getSubTotalPrice(item) {
    let index = this.cartItem.indexOf(item);
    this.cartItem[index].subTotal = getGrossPrice(
      this.cartItem[index].amount *
        getDiscountedNettPrice(this.cartItem[index].product)
    );
  }
  getTotalPrice() {
    this.totalPrice = 0;
    this.cartItem.forEach((item)=> {this.totalPrice += item.subTotal})
    this.totalPrice -= this.promoValue;
    return this.totalPrice;
  }
}

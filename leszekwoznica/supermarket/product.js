var product1 = {
  id: 123,
  name: "Banana Pancakes",
  price: 1410,
  description: "Best pancakes ever",
  promotion: true,
  discount: 0.1,
  image: "https://via.placeholder.com/80",
  date_added: new Date(),
  category: ["pancakes"],
  rating: { votes: 123, rate: 4.5 },
};
var product2 = {
  id: 211,
  name: "carrot",
  price: 1433,
  description: "Polish carrot",
  promotion: true,
  discount: 0.1,
  image: "https://via.placeholder.com/80",
  date_added: new Date(),
  category: ["vegetable"],
  rating: { votes: 123, rate: 4.5 },
};
var product3 = {
  id: 322,
  name: "Applee",
  price: 1000,
  description: "Polish apple",
  promotion: true,
  discount: 0.1,
  tax: 23,
  image: "https://via.placeholder.com/80",
  date_added: new Date(),
  category: ["fruits"],
  rating: { votes: 13, rate: 1.5 },
};

var product6 = {
  id: 622,
  name: "Pear",
  price: 2000,
  description: "Polish pear",
  promotion: false,
  tax: 23,
  image: "https://via.placeholder.com/80",
  date_added: new Date(),
  category: ["pancakes"],
  rating: { votes: 13, rate: 1.5 },
};
//v
//var product4 = Object.assign({},product3)

var product4 = Object.assign({}, product3, {
  rating: Object.assign({}, product2.rating),
  id: 234,
  price: 1620,
  name: "Strawbery",
  description: "German strawbery",
});

var product5 = {
  ...product2,
  rating: { ...product2.rating },
  id: 434,
  price: 10620,
  name: "Chocolate cookies",
};

var mainCart = new Cart();

var promoDiscount = [
  {
    code: "ABC5",
    value: 500,
  },
  {
    code: "ABC10",
    value: 1000,
  },
];
/**
 *
 * numer of product view in product list
 */
var productLimit;
var sortProduct;

var products = [product1, product2, product3, product4, product5, product6];

function getCategoryReturnDescription(description, category) {
  switch (category) {
    case "nalesnik":
      description += " Świetne nalesniki ";
      break;
    case "ciastka":
      description += " Pyszne ciastka ";
      break;
    case "pancakes":
      description += " Amerykanskie "; /* + 'Świetne nalesniki '; break; */
      break;
    default:
      description += " - Inne słodkości ";
      return description;
  }
}
function getPromoValue(promoCode) {
  for (let promo of promoDiscount) {
    if (promo.code === promoCode) {
      return promo.value;
    }
  }
}
var numberOfProducts = document.getElementById("numberOfProducts");
numberOfProducts.onclick = function () {
  // productLimit = numberOfProducts.value;
  productLimit = numberOfProducts.value;
  productList.innerHTML = "";
  createHTML();
};
var promotionCheckBox = document.getElementById("promotionCheckBox");
promotion_check_box = promotionCheckBox.checked;
promotionCheckBox.onclick = function () {
  promotion_check_box = promotionCheckBox.checked;
  productList.innerHTML = "";
  createHTML();
};
var sortProductCheckBox = document.getElementById("sortProductCheckBox");
sortProductCheckBox.onclick = function () {
  sortProduct = sortProductCheckBox.checked;
  productList.innerHTML = "";
  createHTML();
};
clearListBtn.onclick = function () {
  productList.innerHTML = "";
};
addProductBtn = document.querySelector(".productList");
addProductBtn.onclick = function (e) {
  mainCart.addProductToCart(e.target.attributes.vaulue.nodeValue);
  createCartHTML();
};
deleteProductBtn = document.querySelector(".list-group");
deleteProductBtn.onclick = function (e) {
  mainCart.removeProductFromCart(e.target.attributes.vaulue.nodeValue);
  createCartHTML();
};
addPromoCodeBtn = document.getElementById("js-add-promo-code");
addPromoCodeBtn.onclick = function () {
  promoCodeTextBox = document.getElementById("js-promo-code");
  //console.log(promoCodeTextBox.value)
  let promoValue = getPromoValue(promoCodeTextBox.value)
  if(promoValue=== undefined){
    promoValue = 0
    mainCart.promoValue = 0
  }
  else
  {
    mainCart.promoValue = promoValue
  }
  jsPromoDiscount.innerHTML = "Promoc code: " + promoCodeTextBox.value + ' ' + roundPrice(promoValue)  ;
  createCartHTML();
  // addPromoCodeBtn.addEventListener('input',function(){addPromoCodeBtn.textContent})
};

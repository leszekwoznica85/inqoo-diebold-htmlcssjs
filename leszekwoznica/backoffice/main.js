class ProductView {
  constructor(selector,listenSelector, model) {
    this.el = document.querySelector(selector);
    this.myClick = document.querySelector(listenSelector);
    if (model) {
      this.listenTo(model);
    }
    this.productId;
  }

  listenTo(model) {
    if (!model instanceof Listenable) {
      throw "Only CalcModel suported";
    }
    this.model = model;
    this.model.addView(this);
    this.render();
  }

  render() {
    this.el.innerHTML = "";
    this.model.products_temp.forEach((product) => {
      this.el.innerHTML += `
 <div>
    <div class="col-sm" data-product-id="${product.id}">
      <h3>${product.name}</h3>
    </div>
  <div class="col-sm text-right">
  </div>
  <div class="col-sm">
    <p>${product.price_net}</p>
    <div>
    <buttom class="btn">Zmień</buttom>
    </div
</div>
  </div>
  </div>
`;
    });
    /*
    this.myClick.addEventListener("click", (event) => {
      console.log(event.target.closest("[data-product-id]").dataset.productId);
    });
*/
    this.myClick.addEventListener("click", (event) => {

     /* document.querySelectorAll("[data-product-id]").forEach((item) => {console.log(item)
      event.target.closest("[data-product-id]").classList.add("bg-primary");
      */
     //console.log(event.target.closest("[data-product-id]").classList.contains('bg-primary'))
       
      if(event.target.closest("[data-product-id]").classList.contains('bg-primary') === true){//nie odznacza przerobić na inne
          event.target.closest("[data-product-id]").classList.remove("bg-primary");
      }
      else{ 
        event.target.closest("[data-product-id]").classList.add("bg-primary");
        //console.log(event.target.closest("[data-product-id]").dataset.productId)
        this.productId = event.target.closest("[data-product-id]").dataset.productId
        const app = new ApplicationControler() //tak nie wolno robić
console.log(this.productId)
app.productEditor.setData(myProductModel.products.filter((product)=> { return product.id=== parseInt(this.productId)}))
      }
      
     
      /*
      let ele = event.target.closest("[data-product-id]")
      console.log(ele);
      let status = ele.classList.contains('active')
      console.log(status);
      */

    });
    //myClick.addEventListener('click',(event) => event.target.closest('[data-product-id]').classList.add("bg-primary"))
  }
}

class Listenable {
  listeners = [];
  addView(view) {
    this.listeners.push(view);
  }
  update() {
    this.listeners.forEach((list) => list.render());
  }
}
class ProductModel extends Listenable {
  product1 = {
    id: 123,
    name: "Banana",
    price_net: 1410,
    promotion: true,
    category: "fruit",
  };
  product2 = {
    id: 124,
    name: "Apple",
    price_net: 1510,
    promotion: true,
    category: "fruit",
  };
  product3 = {
    id: 125,
    name: "carrot",
    price_net: 1610,
    promotion: false,
    category: "vegetables",
  };

  products = [this.product1, this.product2, this.product3];
  products_temp = this.products;

  promotion(value) {
    return value.promotion === true;
  }
  addProduct(produkt) {
    this.products_temp = this.products;
    this.products_temp.push(produkt);
    console.log(this.products_temp);
    this.update();
  }
  promocja() {
    loadJSON("product.json", (resp) => {
      this.products = resp;

      this.products_temp = this.products.filter((value) => {
        return value.promotion === true;
      });

      this.update();
    });
  }
}
var myProductModel = new ProductModel();
var myProductView = new ProductView(".js-product",".js-product", myProductModel);



//myClick.addEventListener('click',(event) =>  console.log(event.target.closest('[data-product-id]').dataset.productId) )

//myClick.addEventListener('click',(event) => event.target.closest('[data-product-id]').classList.add("bg-primary"))

function loadJSON(url, callback) {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", url);
  xhr.addEventListener("loadend", (event) => {
    const data = JSON.parse(event.target.responseText);
    //  console.log(3)
    callback(data);
  });
  xhr.send();

  //  console.log(1)
  //  return data; // data does not exist yet
}

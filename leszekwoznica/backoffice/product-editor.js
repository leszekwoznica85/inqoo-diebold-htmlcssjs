class ProductEditorView {
    constructor(selector){
        console.log(selector)
        this.el = document.querySelector(selector)
        this.form = this.el.querySelector('form')
    }
    /**
     * 
     * @type Product  
     * @protected
     */
    _data
    
    
    /**
     * @param {Product} data
     */
    setData(data){
        this._data = data
        this.form.elements['name'].value = this._data[0]['name']
        this.form.elements['price'].value = (this._data[0]['price_net']/100).toFixed(2)
    }
    /**
     * @return Product
     */
    getData(){
        this._data['name'] = this.form.elements['name'].value
        return this._data
    }
    
}
class ApplicationControler{
    productEditor = new ProductEditorView("#product-editor")
    constructor(){

    }
    
}
class ProductSelectedEvent extends Event {
    constructor(type, productId) {
      super(type);
      this.productId = productId
    }
  }



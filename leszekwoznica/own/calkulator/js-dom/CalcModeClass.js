class CalcModel extends Listenable {
  result = "0";
  firstNumber = 0;
  operation;
              
  onClick(symbol) {
    if (isNaN(symbol)) {
      if (!isNaN(parseInt(this.result))) this.result = parseInt(this.result);
      switch (this.operation) {
        case "+":
          this.add(parseInt(this.firstNumber));
          break;
        case "-":
          this.sub(parseInt(this.firstNumber));
          break;
        case "*":
          this.mul(parseInt(this.firstNumber));
          break;
        case "/":
          this.div(parseInt(this.firstNumber));
          break;
      }
      if (!isNaN(parseInt(this.result)))
        this.firstNumber = parseInt(this.result);
      this.update();
      this.result = "";
      this.operation = symbol;
    } else {
      this.result === "0" ? (this.result = symbol) : (this.result += symbol);
      this.update();
    }
  }

  add(num) {
    this.result += num;
  }

  sub(num) {
    this.result = num - this.result;
  }
  mul(num) {
    this.result *= num;
  }
  div(num) {
    this.result = num / this.result;
  }
}

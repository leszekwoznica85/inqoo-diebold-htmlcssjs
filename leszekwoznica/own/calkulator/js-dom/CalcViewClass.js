class CalcView {

    constructor(cssIdResult,cssClassButton, model) {
        this.cssIdResult = cssIdResult
        this.cssClassButton = document.querySelector(cssClassButton)
        if (model) {
            this.listenTo(model)
        }
    }

    listenTo(model) {
        if (!model instanceof Listenable) { throw 'Only CalcModel suported' }
        this.model = model;
        this.model.addView(this)
        this.render()
    }

    render() {
        this.cssIdResult.innerHTML = this.model.result
        this.cssClassButton.onclick = (event) => calcModel.onClick(event.target.attributes.value.nodeValue)
    }
}
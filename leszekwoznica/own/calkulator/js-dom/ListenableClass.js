class Listenable {
    listeners = []
    addView(view){
        this.listeners.push(view)
    }
    update(){
        this.listeners.forEach( element => element.render())
    }
}
class Calculator {
  constructor(cssButtonEl) {
    this.cssButtonEl = cssButtonEl;
    this.calcButton = document.querySelector(this.cssButtonEl);
    console.log(this.cssButtonEl);
    this.calcOperation = new CalcOperation();
    this.incoming;
    this.firstNumber = 0;
    this.mode;
    console.log(this.calcView);
  }
  calcClick() {
    this.calcButton.onclick = function (e) {
  
      if (!isNaN(e.target.attributes.value.nodeValue)) {
          this.incoming = ( this.incoming === undefined) ? e.target.attributes.value.nodeValue : this.incoming += e.target.attributes.value.nodeValue;
              } else {
        if (this.incoming !== undefined) {
          if (this.firstNumber === undefined) {
            this.firstNumber = parseInt(this.incoming);
          }
          
          if (e.target.attributes.value.nodeValue === "+") {
            this.mode = e.target.attributes.value.nodeValue;
            this.incoming = "";
          }
          if (e.target.attributes.value.nodeValue === "=") {
            this.calcOperation = new CalcOperation(
              this.firstNumber,
              parseInt(this.incoming),
              this.mode
            );
            this.incoming = this.calcOperation.calculateResult();
            this.mode = undefined;
            this.firstNumber=undefined;
          }
        }
      }
      this.myCalcView = new CalcView(calcResult);
      this.myCalcView.render(this.incoming);
      if (e.target.attributes.value.nodeValue === "="){
        this.incoming = "";
      }
    };
  }
}
